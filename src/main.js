import Vue from 'vue'
import App from './App.vue'
import VueRouter from "vue-router";
import {routes} from "./routes";
import store from "./store/store";
import VueResource from "vue-resource"

Vue.use(VueRouter);
Vue.use(VueResource);

Vue.http.options.root = "https://apps-99427.firebaseio.com/vue/";

Vue.filter('currency', (value) => {
  return '$' + value.toLocaleString();
})

const router = new VueRouter({
  mode: "history", // to remove the hash tag on the URLs
  routes
})

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
