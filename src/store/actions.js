import Vue from "vue";

export const loadData = ({commit}) => {
    Vue.http.get('data.json')
        .then(response => response.body)
        .then(data => {

            if(data) {
                const stocks = data.stocks;
                const funds = data.funds;

                const stockPortfolio = data.stockPortfolio

                commit('SET_STOCKS', stocks)
                commit('SET_PORTFOLIO', {
                    funds,
                    stockPortfolio
                })
            }
        })
}