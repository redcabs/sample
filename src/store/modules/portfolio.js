const state = {
    funds: 1000,
    stocks: []
}

const getters = {
    stockPortfolio (state, getters) {
        // it can receive getters as arg, witch will expose all getters in various different modules

        // map mutates or returns a new set of array from a given array

        return state.stocks.map(stock => {
            // this getters.stocks is coming from the getters of the portfolio module
           const record = getters.stocks.find(element => element.id === stock.id)
           return {
               id: stock.id,
               quantity: stock.quantity,
               name: record.name,
               price: record.price
           }
        })
    },

    funds: state => {
        return state.funds;
    }
}

const  mutations = {

    'BUY_STOCK' (state, {stockId, quantity, stockPrice}){
        // {stockId, quantity, stockPrice} will pull out the properties from the object passed to this argument
        const record = state.stocks.find(stock => stock.id === stockId)
        if(record){
            record.quantity += quantity;
        }else{
            state.stocks.push({
                id: stockId,
                quantity: quantity,
            })
        }

        state.funds -= stockPrice * quantity;
    },

    SELL_STOCK: (state, {stockId, quantity, stockPrice}) => {
        const record = state.stocks.find(stock => stock.id === stockId)
        if(record && record.quantity > quantity){
            record.quantity -= quantity;
        }else{
            // remove item from array with splice
            state.stocks.splice(state.stocks.indexOf(record), 1)
        }

        state.funds += stockPrice * quantity;
    },

    'SET_PORTFOLIO' (state, {funds, stockPortfolio}) {
        state.stocks = stockPortfolio ? stockPortfolio : [];
        state.funds = funds
    }

}

const actions = {

    sellStock({commit}, order){
        commit('SELL_STOCK', order)
    }

}

export default {
    state,
    getters,
    mutations,
    actions
}